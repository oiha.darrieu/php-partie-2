<?php

namespace App\POO\Ex06;

class UnholyFactory
{
    public $truc = [];

    public function absorb($type)
    {
        if (!$type instanceof Fighter && !property_exists($type, 'nom')) {
            echo "(Factory can't absorb this, it's not a fighter)" . "\n";

            return $this;
        }
        foreach ($this->truc as $bla) {
            if ($bla->nom == $type->nom) {
                echo "(Factory already absorbed a fighter of type $type->nom)" . "\n";

                return $this;
            }
        }

        array_push($this->truc, $type);
        echo '(Factory absorbed a fighter of type ' . $type->nom . ")\n";

        return $this;
    }

    public function fabricate($type)
    {
        foreach ($this->truc as $fight) {
            if ($fight->nom == $type) {
                echo "(Factory fabricates a fighter of type $type)" . PHP_EOL;

                return $fight;
            }
        }
        echo "(Factory hasn't absorbed any fighter of type $type)" . PHP_EOL;

        return null;
    }
}
