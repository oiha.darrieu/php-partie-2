<?php

namespace App\POO\Ex06;

abstract class Fighter
{
    public $nom;

    public function __construct($type)
    {
        $this->nom = $type;
    }

    abstract public function fight();
}
