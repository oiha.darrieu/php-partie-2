<?php

namespace App\POO\Ex00;

use App\Resources\Classes\Lannister\Lannister;
use App\Resources\Classes\Stark\Sansa;

class Tyrion extends Lannister
{
    public const SIZE = 'Short';
    public const BIRTH_ANNOUNCEMENT = "My name is Tyrion\n";

    public function announceBirth(): void
    {
        if ($this->needBirthAnnouncement) {
            echo parent::BIRTH_ANNOUNCEMENT . self::BIRTH_ANNOUNCEMENT;
        }
    }

    public function sleepWith($nom)
    {
        if ($nom instanceof Lannister) {
            echo "Not even if I'm drunk !\n";
        } elseif ($nom instanceof Sansa) {
            echo "Let's do this.\n";
        }
    }
}
