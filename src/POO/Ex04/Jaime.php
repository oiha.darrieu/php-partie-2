<?php

namespace App\POO\Ex04;

use App\POO\Ex00\Tyrion;
use App\Resources\Classes\Lannister\Cersei;
use App\Resources\Classes\Lannister\Lannister;

class Jaime extends Lannister
{
    public function sleepWith($nom)
    {
        if ($nom instanceof Tyrion) {
            echo "Not even if I'm drunk !\n";
        } elseif ($nom instanceof Cersei) {
            echo "With pleasure, but only in a tower in Winterfell, then.\n";
        } else {
            echo "Let's do this.\n";
        }
    }
}
