<?php

namespace App\POO\Ex05;

class NightsWatch implements IFighter
{
    public function fight()
    {
    }

    public function recruit($nom)
    {
        if (method_exists($nom, 'fight')) {
            echo $nom->fight();
        }
    }
}
