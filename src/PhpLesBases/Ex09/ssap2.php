<?php

// crér une grande chaine de caractère avec des espaces pour séparateur; array_slice= prendre le tableau à partir de l'index indiqué
$str = implode(' ', array_slice($argv, 1));

// Toujours créer une variable pour encadréer le implode ou explode
$var = explode(' ', $str);

foreach ($var as $num) {
    if (is_numeric($num) == true) {
        $numeric[] = $num;
    }
}
sort($numeric, SORT_STRING);

foreach ($var as $abc) {
    if (ctype_alpha($abc) == true) {
        $string[] = $abc;
    }
}
sort($string, SORT_STRING | SORT_FLAG_CASE);

$carspe = [];

foreach ($var as $car) {
    if ($car != null) {
        if (ctype_alpha($car) == false && is_numeric($car) == false) {
            $carspe[] = $car;
            sort($carspe);
        }
    }
}

foreach ($string as $stri) {
    echo "$stri\n";
}
foreach ($numeric as $number) {
    echo "$number\n";
}

if (!empty($carspe)) {
    foreach ($carspe as $specar) {
        echo "$specar\n";
    }
}
