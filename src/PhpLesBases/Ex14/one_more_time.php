<?php

/*$jour = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
$mois = ['Janvier' => 1, 'Fevrier' => 2, 'Mars' => 3, 'Avril' => 4, 'Mai' => 5, 'Juin' => 6, 'Juillet' => 7, 'Aout' => 8, 'Septembre' => 9, 'Octobre' => 10, 'Novembre' => 11, 'Decembre' => 12];

$arg = array_slice($argv, 1);

foreach ($arg as $tab) {
    $res = explode(' ', $tab);
    if (!empty($res[0])) {
        $res0 = ucfirst(strtolower($res[0]));
        if ($res0 == in_array($res0, $jour)) {
            if (is_numeric($res[1])) {
                if ($res[1] > 0 && $res[1] < 31) {
                    $res2 = ucfirst(strtolower($res[2]));
                    if ($res2 == array_key_exists($res2, $mois)) {
                        $resheure = explode(':', $res[4]);

                        if ($resheure[0] < 24 && $resheure[1] < 60 && $resheure[2] < 60) {
                            $Chiffrejour = $res[1];
                            $Chiffremois = $mois[$res2];
                            if (checkdate($Chiffremois, $Chiffrejour, $res[3])) {
                                $result = true;
                                array_push($arg, 'heure normale d’Europe centrale');
                                $date = implode(' ', $arg);
                                $parser = new IntlDateFormatter(
                                'fr_FR',
                                IntlDateFormatter::FULL,
                                IntlDateFormatter::FULL
                            );
                                $tsparis = $parser->parse($date);

                                echo $tsparis . "\n";
                            } else {
                                echo "Wrong Format\n";
                            }
                        } else {
                            echo "Wrong Format\n";
                        }
                    } else {
                        echo "Wrong Format\n";
                    }
                } else {
                    echo "Wrong Format\n";
                }
            } else {
                echo "Wrong Format\n";
            }
        } else {
            echo "Wrong Format\n";
        }
    } else {
        exit;
    }
}
*/
const PATTERN = "/^([A-Za-z]+) (\d{1,2}) ([A-Za-z]+) (\d{4}) (\d{2}):(\d{2}):(\d{2})$/";
const WRONG_FORMAT = 'Wrong Format';
const DAYS = [
    'lundi',
    'mardi',
    'mercredi',
    'jeudi',
    'vendredi',
    'samedi',
    'dimanche',
];
const MONTHS = [
    'janvier',
    'fevrier',
    'mars',
    'avril',
    'mai',
    'juin',
    'juillet',
    'aout',
    'septembre',
    'octobre',
    'novembre',
    'decembre',
];

function isValidDate(string $date): bool
{
    $matches = [];

    if (!preg_match(PATTERN, $date, $matches)) {
        return false;
    }

    [, $day, $dayNumber, $month, $year, $hour, $minute, $second] = $matches;
    $monthIndex = array_search(strtolower($month), MONTHS);

    return in_array(strtolower($day), DAYS)
        && ($monthIndex !== false && checkdate($monthIndex + 1, $dayNumber, $year))
        && ($hour >= 0 && $hour < 24)
        && ($minute >= 0 && $minute < 60)
        && ($second >= 0 && $second < 60);
}

if ($argc !== 2 || empty($argv[1])) {
    return;
}

$result = WRONG_FORMAT;

if (isValidDate($argv[1])) {
    $result = IntlDateFormatter::create('fr_FR')
        ->parse("$argv[1] heure normale d’Europe centrale");
}

echo "$result\n";
